Incoming module allows you to be alerted when there is a significant change in the amount of traffic coming to your site from an outside source. It's main purpose is to give you a heads up if your site is about to be featured on Digg, Slashdot, or any other high traffic site so you're not caught unprepared. 

Be warned, if you're unsure that your server will be able to handle a giant surge in traffic, make sure you don't set the "Alert Threshold" too high, or the alert may not get a chance to be sent out before your server becomes unresponsive.

I've attempted to make this module as lightweight as possible, so it doesn't create any of it's own tables, but instead monitors the sessions table to detect changes in visitor load.

Getting Started:
Enable the module and visit Administer -> Site Configuration  -> Incoming.

The "Alert Threshold" lets you set the amount of new visitors to your site needed to trigger an alert. This number is not the total amount of visitors on your site, but the change in the amount of visitors on your site during a specific "timespan".

The "Timespan" is the amount of time in which a change in visitors is measured. In effect, the module takes a snapshot of the activity on your site over a specified period of time, and monitors for increases in anonymous users on your site.

For example, if you set your "Alert Threshold" to 50 and your "Timespan" to  10 minutes the possible outcomes are this.

Example 1:
First ten minutes: 25 visitors.
Second ten minutes: 25 visitors.
Total visitor change between timespans is 0. No alert will be sent.

Example 2:
First ten minutes: 300 visitors.
Second ten minutes: 340 visitors.
Total visitor change between timespans is 40. No alert will be sent.

Example 3:
First ten minutes: 30 visitors.
Second ten minutes: 85 visitors.
Total visitor change between timespans is 55. an alert will be sent.

"Alert suppression" allows you to manage how much time passes between alerts.

If you have "Access Logging" enabled through the statistics module, the alerts sent to you will include a list of urls telling you where most of your recent traffic is coming from.


Developed by Scott Falconer for bantler.com

