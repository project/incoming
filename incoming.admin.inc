<?php

/**
 * @file
 * Admin page callbacks for the incoming module.
 */

/**
 * Form builder; Configure the incoming system.
 *
 * @ingroup forms
 * @see system_settings_form()
 * @see incoming_admin_settings_validate()
 */
function incoming_admin_settings() {
  $probabilities = array(0 => '100%', 1 => '50%', 2 => '33.3%', 3 => '25%', 4 => '20%', 5 => '16.6%', 7 => '12.5%', 9 => '10%', 19 => '5%', 99 => '1%', 199 => '.5%', 399 => '.25%', 989 => '.1%');
  $timespan = drupal_map_assoc(array(120, 180, 240, 300, 600, 900, 1800, 3600), 'format_interval');  
  $suppression = drupal_map_assoc(array(180, 600, 3600, 7200, 18000, 36000, 72000, 86400), 'format_interval'); 
  
  $form['incoming_alert_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Alert threshold'),
    '#default_value' => variable_get('incoming_alert_threshold', 10),
    '#size' => 5,
    '#maxlength' => 6,
    '#description' => t(' The amount of new visitors to your site in the specified timespan needed to trigger an alert. Be warned, if you set the number too high your server may crash under heavy load before the alert can be sent.')
  );
  $form['incoming_time_period'] = array(
    '#type' => 'select',
    '#title' => t('Timespan'),
    '#default_value' => variable_get('incoming_time_period', 600),
    '#options' => $timespan,
    '#description' => t('The timespan sample to compare the level of visitors to your site against. As an example, 10 minutes will compare the number of visitors to your site over the last 10 minutes to the number of visitors to the site in the previous 10 minutes. If this number is higher than the alert threshold you have set above, an alert will be sent.')
  );
  $form['incoming_alert_suppression'] = array(
    '#type' => 'select',
    '#title' => t('Alert suppression'),
    '#default_value' => variable_get('incoming_alert_suppression', 7200),
    '#options' => $suppression,
    '#description' => t('The amount of time after receiving an alert until you are alerted again to changes in the traffic status of your site.')
  );
  $form['incoming_probability_limiter'] = array(
    '#type' => 'select',
    '#title' => t('Probability limiter'),
    '#default_value' => variable_get('incoming_probability_limiter', 9),
    '#options' => $probabilities,
    '#description' => t('The probability limiter is an efficiency mechanism to statistically reduce the overhead of the incoming module. The limiter is expressed as a percentage of page views, so for example if set to the default of 10% we only perform the extra database queries to check the number of new visitors on the site 1 out of every 10 page views. The busier your site, the lower you should set the limiter value. This functionality is based on the Drupal core throttle module.')
  );
  $form['incoming_alert_mail'] = array(
    '#type' => 'textfield',
    '#title' => t("E-mail"),
    '#default_value' => variable_get('incoming_alert_mail', NULL),
    '#description' => t('Alerts will be e-mailed to this address. Leave blank for none. Multiple e-mail addresses may be separated by commas.'),
  );
  

  $form['#validate'] = array('incoming_admin_settings_validate');

  return system_settings_form($form);
}

function incoming_admin_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['incoming_alert_threshold']) || $form_state['values']['incoming_alert_threshold'] < 0) {
    form_set_error('incoming_alert_threshold', t("%value is not a valid setting. Please enter a positive numeric value.", array('%value' => $form_state['values']['incoming_alert_threshold'])));
  }
  $recipients = explode(',', $form_state['values']['incoming_alert_mail']);
  foreach ($recipients as $recipient) {
    if (!valid_email_address(trim($recipient))) {
      form_set_error('incoming_alert_mail', t('%recipient is an invalid e-mail address.', array('%recipient' => $recipient)));
    }
  }
}
